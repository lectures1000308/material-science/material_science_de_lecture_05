## Vorlesung für Werkstofftechnik
Anleitung

* Graphiken sind  erstellt mit [Mermaid](
https://mermaid.js.org/syntax/examples.html)

* [Vorlesungsskript](https://material-science-de-lecture-05-lectures1000308-m-b2a9570fb01aba.gitlab.io/)

## Wie zu zitieren
Über ein bib

 @techreport{WillbergC_material_science_05,
    title       = "{Vorlesung Werkstofftechnik}",
    author      = "Willberg, Christian",
    institution = "Hochschule Magedeburg-Stendal",
    address     = "Magdeburg, Germany",
    number      = "5",
    year        = 2024,
  }